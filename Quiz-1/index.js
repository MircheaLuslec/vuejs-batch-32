// Soal 1
function jumlah_kata(kalimat){
    return console.log(kalimat.trim().split(" ").length);
}

var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = " Saya Iqbal"
var kalimat_3 = " Saya Muhammad Iqbal Mubarok "

jumlah_kata(kalimat_1) // 6
jumlah_kata(kalimat_2) // 2
jumlah_kata(kalimat_3) // 4

// Soal 2
function next_date(tanggal, bulan, tahun){
    var tanggal_esok;
    var bulan_esok;
    var tahun_esok;
    
    
    if (bulan == 2){
        if (tahun % 4 == 0){
            if (tanggal == 28){
                tanggal_esok = 29;
                bulan_esok =  bulan;
            } else if (tanggal == 29){
                tanggal_esok = 1;
                bulan_esok = 3;
            } 
        } else {
            if (tanggal == 28){
                tanggal_esok = 1;
                bulan_esok = 3;
            } else {
                tanggal_esok = tanggal + 1;
            }
        }
        tahun_esok = tahun;
    } else if (bulan == 12){
        if (tanggal == 31){
            tanggal_esok = 1
            bulan_esok = 1
            tahun_esok = tahun + 1;
        } else {
            tanggal_esok = tanggal + 1
            bulan_esok = bulan;
            tahun_esok = tahun;
        }
    } else {
        if (bulan == 1 || bulan == 3 || bulan == 5 || bulan == 7 || bulan == 8 || bulan == 10){
            if (tanggal == 31){
                tanggal_esok = 1
                bulan_esok = bulan + 1
            } else {
                tanggal_esok = tanggal + 1
                bulan_esok = bulan;
            }
        } else {
            if (tanggal == 30){
                tanggal_esok = 1
                bulan_esok = bulan + 1
            } else {
                tanggal_esok = tanggal + 1
                bulan_esok = bulan;
            }
        }
        tahun_esok = tahun;
    }
    
    switch(bulan_esok) {
        case 1:
            bulan_esok = 'Januari'
            break;
        case 2:
            bulan_esok = 'Februari'
            break;
        case 3:
            bulan_esok = 'Maret';
            break;
        case 4:
            bulan_esok = 'April';
            break;
        case 5:
            bulan_esok = 'Mei';
            break;
        case 6:
            bulan_esok = 'Juni';
            break;
        case 7:
            bulan_esok = 'Juli';
            break;
        case 8:
            bulan_esok = 'Agustus';
            break;
        case 9:
            bulan_esok = 'September';
            break;
        case 10:
            bulan_esok = 'Oktober';
            break;
        case 11:
            bulan_esok = 'November';
            break;
        case 12:
            bulan_esok = 'Desember';
            break;
        default:
            bulan_esok = 'Error';
}
    console.log(tanggal_esok + " " + bulan_esok + " " + tahun_esok);
}

// contoh 1

var tanggal = 29
var bulan = 2
var tahun = 2020

next_date(tanggal , bulan , tahun ) // output : 1 Maret 2020

// contoh 2

var tanggal = 28
var bulan = 2
var tahun = 2021

next_date(tanggal , bulan , tahun ) // output : 1 Maret 2021

// contoh 3

var tanggal = 31
var bulan = 12
var tahun = 2020

next_date(tanggal , bulan , tahun ) // output : 1 Januari 2021