// Soal 1
let luasDanKelilingPP = (panjang, lebar) => {
    let luasPP = panjang * lebar;
    let kelilingPP = 2 * (panjang + lebar);

    console.log(`Luas Persegi Panjang: ${luasPP}`);
    console.log(`Keliling Persegi: ${kelilingPP}`);
};

luasDanKelilingPP(10, 6);

// Soal 2
const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName: () => {
            console.log(`${firstName} ${lastName}`);
        }
    }
};
   
//Driver Code 
newFunction("William", "Imoh").fullName();

// Soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
};

let {firstName, lastName, address, hobby} = newObject;

// Driver code
console.log(firstName, lastName, address, hobby);

// Soal 4
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];

//Driver Code
console.log(combined);

// Soal 5
const planet = "earth"; 
const view = "glass" ;
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet;
let after = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}`; 
console.log(after);