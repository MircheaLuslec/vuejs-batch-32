// soal 1
var nilai;
var indeks;

if (nilai >= 85){ 
    indeks = 'A';
} else if (nilai >= 75){
    indeks = 'B';
} else if (nilai >= 65){
    indeks = 'C';
} else if (nilai >= 55){
    indeks = 'D';
} else {
    indeks = 'E';
}

console.log(indeks);

// soal 2
var tanggal = 10;
var bulan = 6;
var tahun = 2002;
var bulanString;

switch (bulan) {
    case 1 : { bulanString = 'Januari'; break; }
    case 2 : { bulanString = 'Februari'; break; }
    case 3 : { bulanString = 'Maret'; break; }
    case 4 : { bulanString = 'April'; break; }
    case 5 : { bulanString = 'Mei'; break; }
    case 6 : { bulanString = 'Juni'; break; }
    case 7 : { bulanString = 'Juli'; break; }
    case 8 : { bulanString = 'Agustus'; break; }
    case 9 : { bulanString = 'September'; break; }
    case 10 : { bulanString = 'Oktober'; break; }
    case 11 : { bulanString = 'November'; break; }
    case 12 : { bulanString = 'Desember'; break; }
    default : { bulanString = 'Error'; }
}

console.log(tanggal.toString() + ' ' + bulanString + ' ' + tahun.toString());

// soal 3
var n;
for (var i = 1; i <= n; i++){
    console.log("#".repeat(i));
}

// soal 4
var m;
for (var i = 1; i <= m; i++){
    if (i % 3 == 1){
        console.log(i + " - I love programming");
    } else if (i % 3 == 2){
        console.log(i + " - I love Javascript");
    } else {
        console.log(i + " - I love VueJS");
        console.log("===".repeat(i/3))
    }
}