// soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();

for (var i = 0; i < daftarHewan.length; i++){
    console.log(daftarHewan[i]);
}

// soal 2
function introduce(data){
    return "Nama saya " + data.name + ", umur saya "+ data.age + ", alamat saya di " + data.address + ", dan saya punya hobby yaitu " + data.hobby;
}
 
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" } 
var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 

// soal 3
function is_huruf_vokal(letter){
    letter = letter.toLowerCase();
    return letter == 'a' || letter == 'i' || letter == 'u' || letter == 'e' || letter == 'o';
}

function hitung_huruf_vokal(word){
    result = 0;
    for (var i = 0; i < word.length; i++){
        if (is_huruf_vokal(word[i])){
            result++;
        }
    }
    return result;
}

var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")
console.log(hitung_1 , hitung_2) // 3 2

// soal 4
function hitung(int){
    return int + (int - 2);
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8